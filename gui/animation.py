import matplotlib.pyplot as plt
from matplotlib.collections import PathCollection
import numpy as np
from matplotlib.animation import FuncAnimation
import commands.command as command
 
#Принимает в себя динамический радиус, набор точек (Количество точек, движение точек, координаты) и цвета точек
class Animation:
    def __init__(self, dynamic_r, commands: list[command.command]):
        self.commands = commands
        self.r = np.array(dynamic_r)

    def animate(self, k):
        self.circle.set_radius(self.r[k])
        for i, com in enumerate(self.commands):
            for j, _ in enumerate(com.fleet):
                self.pathes[i][j].set_offsets(com.hist[j][k])
                self.pathes[i][j].set_label(com.hist_hp[j][k])
        self.legend.remove()
        self.legend = self.ax.legend(loc="lower left", fontsize=8, bbox_to_anchor=(-0.1, -0.1))
        for text in self.legend.get_texts():
            text.set_alpha(0.5)
        return [] 

    def get_animation(self, filename, inter=1):
        self.fig = plt.figure(figsize=(10, 10), dpi=80)
        self.ax = plt.axes(xlim=(-max(self.r), max(self.r)), ylim=(-max(self.r), max(self.r)))
        
        plt.xticks([])
        plt.yticks([])
        for spine in plt.gca().spines.values(): spine.set_visible(False) 

        self.pathes : list[list[PathCollection]] = []
        for com in self.commands:
            self.pathes.append([self.ax.scatter(point[0][0], point[0][1], s=45, c=com.color, label=hp_leg[0]) for hp_leg, point in zip(com.hist_hp, com.hist)])
        self.circle = plt.Circle((0, 0), self.r[0], color='b', fill=False)
        self.ax.add_patch(self.circle)
        self.legend = self.ax.legend(loc="lower left", fontsize=8, bbox_to_anchor=(-0.1, -0.1))

        for text in self.legend.get_texts():
            text.set_alpha(0.5)

        anim = FuncAnimation(self.fig, self.animate, frames=len(self.commands[0].hist[0]), interval=inter, blit=True)
        anim.save(filename, writer='pillow', fps=30)