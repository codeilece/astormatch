import numpy as np
from space.space import cyrcle

class ship_interface():
    def __init__(self, initial_coords = np.zeros(2)):
        self.ship_name = 'FACTORY'
        self.hp = 0
        self.max_hp = 0
        self.damage = 0
        self.speed = 0
        self.radius = 0
        self.coords = initial_coords
        self.cooldown = 0
        self.last_shot = 0
        self.direction = None
        self.inner_time = 0

    def get_coords(self):
        return self.coords

    def move(self, vector, map:cyrcle):
        self.inner_time += 1
        if vector is not None:
            self.direction = np.array(vector)
        cur_vector = self.direction * self.speed
        self.coords, self.direction = map.calc_new_trajectory(self.coords, cur_vector)


    def deal_damage(self, incomming_damage):
        self.hp -= incomming_damage

    def main_search_for_enemies(self, enemy_coords):
        return np.linalg.norm(self.coords - np.array(enemy_coords)) <= self.radius

    def start_fire(self, enemy_obj):
        if self.inner_time - self.last_shot >= self.cooldown:

            if self.main_search_for_enemies(enemy_obj.get_coords()):
                enemy_obj.deal_damage(self.damage)
                self.last_shot = self.inner_time
    
    def __str__(self):
        return f"Type: {self.ship_name}| Сharacteristics: {self.hp} HP, {self.speed} SPEED, {self.damage} DAMAGE, {self.radius} RADIUS"

    def get_bar(self, limit = 10):
        if self.hp <= 0:
            return "DIED"
        points = round(self.hp / self.max_hp * limit)
        res = "".rjust(points, "█")
        res += "".rjust(10 - points, "𝌴")
        return f"{res} {round(self.hp / self.max_hp * 100)}%HP"


    def get_for_legend(self):
        return f"{self.ship_name}: {self.get_bar()}"