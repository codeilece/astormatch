from ships.classes.battleship import battleship
from ships.classes.corvet import corvet
from ships.classes.cruiser import cruiser
from ships.classes.destroyer import destroyer
import numpy as np

class ship_factory():
    def create_ship(str, initial_coords = np.zeros(3)):
        if str == "BATTLESHIP":
            return battleship(initial_coords)
        elif str == "CORVET":
            return corvet(initial_coords)
        elif str == "CRUISER":
            return cruiser(initial_coords)
        elif str == "DESTROYER":
            return destroyer(initial_coords)


ship_factory.create_ship = staticmethod(ship_factory.create_ship)