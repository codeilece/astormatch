import numpy as np
from ships.interface.ship_interface import ship_interface

class cruiser(ship_interface):
    def __init__(self, initial_coords = np.zeros(3)):
        self.ship_name = 'CRUISER'
        self.hp = 1500
        self.max_hp = self.hp
        self.damage = 200
        self.speed = 70
        self.radius = 8
        self.coords = initial_coords
        self.cooldown = 3
        self.direction = None

        self.last_shot = 0
        self.secondary_last_shot = 0
        self.inner_time = 0