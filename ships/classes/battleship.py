import numpy as np
from ships.interface.ship_interface import ship_interface

class battleship(ship_interface):
    def __init__(self, initial_coords = np.zeros(3)):
        self.ship_name = 'BATTLESHIP'
        self.hp = 3000
        self.max_hp = self.hp
        self.speed = 20
        self.damage = 300
        self.radius = 500
        self.coords = initial_coords
        self.cooldown = 5
        self.secondary_cooldown = 1
        self.secondary_damage = 100
        self.secondary_radius = 30    
        self.direction = None

        self.last_shot = 0
        self.secondary_last_shot = 0
        self.inner_time = 0

    def secondary_search_for_enemies(self, enemy_coords):
        return np.linalg.norm(self.coords - np.array(enemy_coords)) <= self.secondary_radius

    def start_fire(self, enemy_obj):
        if self.inner_time - self.last_shot >= self.cooldown:
            if self.main_search_for_enemies(enemy_obj.get_coords()):
                enemy_obj.deal_damage(self.damage)
                self.last_shot = self.inner_time

        if self.inner_time - self.secondary_last_shot >= self.secondary_cooldown:
            if self.secondary_search_for_enemies(enemy_obj.get_coords()):
                enemy_obj.deal_damage(self.secondary_damage)
                self.secondary_last_shot = self.inner_time