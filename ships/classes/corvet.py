import numpy as np
from ships.interface.ship_interface import ship_interface

class corvet(ship_interface):
    def __init__(self, initial_coords = np.zeros(3)):
        self.ship_name = 'CORVET'
        self.hp = 1000
        self.max_hp = self.hp
        self.damage = 100
        self.speed = 100
        self.radius = 5
        self.coords = initial_coords
        self.cooldown = 1
        self.direction = None

        self.last_shot = 0
        self.secondary_last_shot = 0
        self.inner_time = 0