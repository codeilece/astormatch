import json
import numpy as np
from ships.factory.ship_factory import ship_factory
from ships.interface.ship_interface import ship_interface
class command:
    def __init__(self, config_data, radius_from_center, rotate_matrix: np.ndarray):
        self.fleet : list[ship_interface] = [
            ship_factory.create_ship("BATTLESHIP", rotate_matrix.dot(np.array([[0, -25 + radius_from_center]]).T).flatten()),
            ship_factory.create_ship("DESTROYER", rotate_matrix.dot(np.array([[25, radius_from_center]]).T).flatten()),
            ship_factory.create_ship("CRUISER", rotate_matrix.dot(np.array([[-25, radius_from_center]]).T).flatten()),
            ship_factory.create_ship("CORVET", rotate_matrix.dot(np.array([[0 , radius_from_center]]).T).flatten())
        ]


        self.hist = [[] for _ in self.fleet]
        self.hist_hp = [[] for _ in self.fleet]
        self.name = config_data["name"]
        self.proxi_shield = config_data["bonus_imune"]
        self.color = config_data["color"]
        for index in range(len(self.fleet)):
            self.fleet[index].speed *= (1 + 0.25 * config_data["fleet_buffs"]["increased_speed"])
            self.fleet[index].hp *= (1 + 0.5 * config_data["fleet_buffs"]["increased_hp"])
            self.fleet[index].max_hp = self.fleet[index].hp
            self.fleet[index].radius *= (1 + 1.25 * config_data["fleet_buffs"]["increased_radius"])
            self.fleet[index].damage *= (1 + 0.3 * config_data["fleet_buffs"]["increased_damage"])

    def keep_in_hist(self):
        for i, ship in enumerate(self.fleet):
            self.hist[i].append(ship.get_coords().tolist())
            self.hist_hp[i].append(ship.get_for_legend())

        pass
    
    def get_alive_ships(self):
        res : list[ship_interface] = []
        for ship in self.fleet:
            if ship.hp > 0:
                res.append(ship)
            elif self.proxi_shield > 0:
                ship.hp = 1
                self.proxi_shield -= 1
        return res

    def make_move(self, field, direction = None):
        for ship in self.get_alive_ships():
            if direction != None:
                ship.move(direction(), field)
            else:
                ship.move(None, field)
    
    def fire(self, commands):
        evil_ships : list[ship_interface] = []
        for com in commands:
            if not(self is com):
                for ship in com.get_alive_ships():
                    evil_ships.append(ship)
        if len(evil_ships) > 0:
            for ship in self.get_alive_ships():
                evip_ship = min(evil_ships, key=lambda x: np.linalg.norm(x.coords - ship.coords))
                ship.start_fire(evip_ship)
        pass

    def is_alife(self):
        return len(self.get_alive_ships()) > 0
    
    def __str__(self):
        return f"Command {self.name} have four fleets:\n{self.fleet[0]}\n{self.fleet[1]}\n{self.fleet[2]}\n{self.fleet[3]}"
    def __repr__(self):
        return self.__str__()

def command_builder(config_name, spawn_rad):
    commands = []
    with open(config_name, "r") as file:
        data = json.load(file)
        phi = 2 * np.pi / len(data)
        rot = 0
        for el in data:
            commands.append(command(el, spawn_rad, np.array([[np.cos(rot), np.sin(rot)], [-np.sin(rot), np.cos(rot)]])))
            rot += phi  
    return commands      