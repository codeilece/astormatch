from pickletools import optimize
import numpy as np
from scipy.optimize import minimize
import math

class cyrcle:
    def __init__(self, r, decrease = 1.):
        self.r = r
        self.decrease = decrease

    def tick(self):
        self.r *= self.decrease
    
    def calc_new_trajectory(self, point:np.ndarray, v:np.ndarray):
        if np.linalg.norm(point + v) > self.r and np.linalg.norm(point) <= self.r:
            k = minimize(lambda k: (self.r - np.linalg.norm(point + k * v)) ** 2 , 1.).x[0]
            ps = point + k * v
            alpha = np.arctan(ps[1] / ps[0])
            rot = np.array([[math.cos(alpha), math.sin(alpha)],[-math.sin(alpha), math.cos(alpha)]])
            v = rot.dot(v)
            v[0] = -v[0]
            v = rot.T.dot(v)
            return ps + (1 - k) * v, v / np.linalg.norm(v)
        
        if np.linalg.norm(point) > self.r:
            if np.linalg.norm(point) < np.linalg.norm(point + v):
                alpha = np.arctan(point[1] / point[0])
                rot = np.array([[math.cos(alpha), math.sin(alpha)],[-math.sin(alpha), math.cos(alpha)]])
                v = rot.dot(v)
                v[0] = -v[0]
                v = rot.T.dot(v)
                ps = (point + v) * (self.r / np.linalg.norm(point + v))
                return ps, v / np.linalg.norm(v)
            return point + v, v / np.linalg.norm(v)

        return point + v, v / np.linalg.norm(v)
    
    def get_r(self):
        return self.r