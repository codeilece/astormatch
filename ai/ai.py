import numpy as np
class ai:
    def __init__(self):
        pass
    
    def make_move(self):
        move = np.random.rand(2) - 0.5
        move /= np.linalg.norm(move)
        return move
