from space.space import cyrcle
from ai.ai import ai
from gui.animation import Animation
import commands.command as command

class Core:
    def __init__(self, field : cyrcle, commands : list[command.command], ai : ai):
        self.field = field
        self.commands = commands
        self.ai = ai
        self.radius = []

    def start(self):
        time = 0

        self.radius.append(self.field.get_r())

        while True:
            #STAGE 0: DECREASE FIELD#
            self.field.tick()
            #STAGE 1: MOVE#
            if time == 0:
                for com in self.commands:
                    com.make_move(self.field, self.ai.make_move)
            else:
                for com in self.commands:
                    com.make_move(self.field)
                    
            
            #STAGE 2: FIRE#
            for com in self.commands:
                com.fire(self.commands)
            

            #STAGE 3: RECORD TO GRAPHIC
            for com in self.commands:
                com.keep_in_hist()
            self.radius.append(self.field.get_r())

            alife_commands = 0
            for com in self.commands:
                if com.is_alife():
                    alife_commands += 1
            time += 1
            if time == 1000 or alife_commands == 1:
                break
        for i in range(120):
            for com in self.commands:
                com.keep_in_hist()
            self.radius.append(self.field.get_r())
    pass

    def get_battle_move(self):
        anim = Animation(self.radius, self.commands)
        anim.get_animation("out.gif")
        pass